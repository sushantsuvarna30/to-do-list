//
//  CategoryCollectionViewCell.swift
//  To-Do
//
//  Created by Sushanth S on 24/09/21.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var percentageIndicatorView: UIView!
    
    @IBOutlet weak var percentage: UILabel!
    @IBOutlet weak var CategoryName: UILabel!
    @IBOutlet weak var categoryIndicator: UILabel!
    @IBOutlet weak var taskCompleted: UILabel!
    @IBOutlet weak var addTask: UIButton!
}
