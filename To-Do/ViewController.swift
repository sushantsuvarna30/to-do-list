//
//  ViewController.swift
//  To-Do
//
//  Created by Sushanth S on 14/09/21.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var yellowView: UIView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var reportLabel: UILabel!
    @IBOutlet weak var newCategory: UIButton!
    @IBOutlet weak var categoryList: UICollectionView!
    @IBOutlet weak var categoryCellView: UIView!
    @IBOutlet weak var importantTaskTable: UITableView!
  
    
    
    var shapeLayer = CAShapeLayer()
    override func viewDidLoad() {
        super.viewDidLoad()
        profileImage.layer.cornerRadius = profileImage.frame.height / 2
        profileImage.clipsToBounds = true
        outerView.viewSetUp()
        innerView.viewSetUp()
        reportLabel.labelTransform()
        newCategory.tintColor = UIColor(red: 253/255, green: 211/255, blue: 37/255, alpha: 1)
        outerView.addShadow(offset: CGSize(width: 1, height: 1), color: .gray, radius: 10, opacity: 0.6)
        //weeklyReport()
        shapeLayer = Utilities.percentageIndicator(arcCenterXPoint: innerView.frame.width / 3.5 - 20, arcCenterYPoint: innerView.frame.height / 2, radius: 40, startAngle: 0, endAngle: 2 * CGFloat.pi, clockwise: false)
        categoryList.dataSource = self
        categoryList.delegate = self
        importantTaskTable.dataSource = self
        importantTaskTable.delegate = self
        innerView.layer.addSublayer(shapeLayer)
        displayPercentage()

    }

   
    func displayPercentage() {
     
        
        let percentage = UILabel(frame: CGRect(x: 0, y: 0, width: 50, height: 21))
        percentage.text = " 50%"
        percentage.center = CGPoint(x: innerView.frame.width / 3.25 - 20, y: innerView.frame.height / 2)
        innerView.addSubview(percentage)
       // shapeLayer.add(Utilities.animation(value: 0.7), forKey: "someKey")
    }
    
    func percentageIndicator() {
       
            let shapeLayer = CAShapeLayer()
        let circlePath = UIBezierPath(arcCenter: CGPoint(x: 100, y: 100), radius: 100, startAngle: -CGFloat.pi / 2, endAngle: 2 * CGFloat.pi, clockwise: true)
            shapeLayer.path = circlePath.cgPath
            shapeLayer.strokeColor = UIColor.green.cgColor
            shapeLayer.lineWidth = 10
            shapeLayer.strokeEnd = 0
            
        
            view.layer.addSublayer(shapeLayer)
            let basicAnimation = CABasicAnimation(keyPath: "strokeEnd")
        basicAnimation.toValue = 0.8
            basicAnimation.duration = 2
        basicAnimation.fillMode = CAMediaTimingFillMode.forwards
            basicAnimation.isRemovedOnCompletion = true
            shapeLayer.add(basicAnimation, forKey: "someKey")
        
      
        basicAnimation.toValue = 0
        basicAnimation.duration = 3
        basicAnimation.fillMode = CAMediaTimingFillMode.backwards
        basicAnimation.isRemovedOnCompletion = true
        shapeLayer.add(basicAnimation, forKey: "someLeiu")
        basicAnimation.toValue = 0.8
        basicAnimation.duration = 3
        basicAnimation.isRemovedOnCompletion = false
        basicAnimation.fillMode = CAMediaTimingFillMode.forwards
        shapeLayer.add(basicAnimation, forKey: "someLeiu")
        

    }

    @IBAction func newCategory(_ sender: UIButton) {
    }
    
}

extension ViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        super.viewWillTransition(to: size, with: coordinator)
        guard let flowLayout = categoryList.collectionViewLayout as? UICollectionViewFlowLayout else {
            
            return
        }
       
        flowLayout.invalidateLayout()
        categoryList.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
      
 
        return CGSize(width: view.frame.width / 5, height: 140)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? CategoryCollectionViewCell{
            //cell.name.text = "Suh"
            cell.layer.borderColor = UIColor.black.cgColor
            cell.layer.cornerRadius = 5
            cell.addShadow(offset: CGSize(width: 1, height: 1), color: .gray, radius: 5, opacity: 0.6)
            cell.backgroundColor = UIColor.white
            let shapeLayer = Utilities.percentageIndicator(arcCenterXPoint: cell.percentageIndicatorView.frame.width / 2 , arcCenterYPoint: cell.percentageIndicatorView.frame.width / 2 , radius: Int(cell.percentageIndicatorView.frame.width / 2), startAngle: 0, endAngle: 2 * CGFloat.pi, clockwise: false)
            cell.percentageIndicatorView.layer.addSublayer(shapeLayer)
            let percentage = UILabel(frame: CGRect(x: 0, y: 0, width: 50, height: 21))
            percentage.text = "  50%"
            percentage.center = CGPoint(x: cell.percentageIndicatorView.frame.width / 2, y: cell.percentageIndicatorView.frame.width / 2)
            cell.percentageIndicatorView.addSubview(percentage)
            cell.CategoryName.text = "Educational"
            cell.addTask.tintColor = .yellow
            
            
            return cell
        }
        return CategoryCollectionViewCell()
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    
    
}
extension ViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "importantTasks", for: indexPath) as? ImportantTaskTableViewCell{
           
            
            return cell
        }
        return ImportantTaskTableViewCell()
    }
    
    
    
}

