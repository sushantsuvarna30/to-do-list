//
//  File.swift
//  To-Do
//
//  Created by Sushanth S on 24/09/21.
//

import Foundation
import UIKit

class Utilities {
    
    static func percentageIndicator(arcCenterXPoint: CGFloat, arcCenterYPoint: CGFloat, radius: Int, startAngle: CGFloat, endAngle: CGFloat, clockwise: Bool) -> CAShapeLayer {
        
        let shapeLayer = CAShapeLayer()
        let circlePath = UIBezierPath(arcCenter: CGPoint(x: arcCenterXPoint, y: arcCenterYPoint), radius: CGFloat(radius), startAngle: 0, endAngle: 2 * CGFloat.pi, clockwise: false)
        shapeLayer.path = circlePath.cgPath
        shapeLayer.strokeColor = UIColor.orange.cgColor
        shapeLayer.lineWidth = 5
        shapeLayer.strokeEnd = 0
        shapeLayer.fillColor = UIColor.white.cgColor
        
      
        shapeLayer.add(Utilities.animation(value: 1), forKey: "someKey")
        return shapeLayer
       
    }
    
    static func animation(value: CGFloat) -> CABasicAnimation {
        
        let basicAnimation = CABasicAnimation(keyPath: "strokeEnd")
        basicAnimation.toValue = value
        basicAnimation.duration = 2
        basicAnimation.fillMode = CAMediaTimingFillMode.forwards
        basicAnimation.isRemovedOnCompletion = false
        
        return basicAnimation
        
    }
}
