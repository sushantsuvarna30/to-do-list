//
//  Constants.swift
//  To-Do
//
//  Created by Sushanth S on 11/10/21.
//

import Foundation

enum Category: String, CaseIterable {
    
    case Educational = "Educational"
    case Personal = "Personal"
    case Healthy = "Health"
    case Work = "Work"
}
