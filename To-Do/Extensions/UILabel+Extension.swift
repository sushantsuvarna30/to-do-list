//
//  UILabel+Extension.swift
//  To-Do
//
//  Created by Sushanth S on 24/09/21.
//

import Foundation
import  UIKit

extension UILabel {
    
    func labelTransform() {
        
        self.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 2)
    }
}
