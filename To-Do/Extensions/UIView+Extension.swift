//
//  UIView+Extension.swift
//  To-Do
//
//  Created by Sushanth S on 24/09/21.
//

import Foundation
import UIKit

extension UIView {
    
    func viewSetUp() {
        
        self.layer.cornerRadius = 20
        self.clipsToBounds = true
    }
    
    func addTaskViewsetup() {
        
        self.layer.cornerRadius = 50
        self.clipsToBounds = true
        self.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
    }
    
    
    func addShadow(offset: CGSize, color: UIColor, radius: CGFloat, opacity: Float) {
        layer.masksToBounds = false
        layer.shadowOffset = offset
        layer.shadowColor = color.cgColor
        layer.shadowRadius = radius
        layer.shadowOpacity = opacity

        let backgroundCGColor = backgroundColor?.cgColor
        backgroundColor = nil
        layer.backgroundColor =  backgroundCGColor
    }
    
    
}
