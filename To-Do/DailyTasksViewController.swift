//
//  DailyTasksViewController.swift
//  To-Do
//
//  Created by Sushanth S on 13/10/21.
//

import UIKit

class DailyTasksViewController: UIViewController {

    @IBOutlet weak var upcomingTasks: UITableView!
    @IBOutlet weak var calender: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        calender.dataSource = self
        calender.delegate = self
        upcomingTasks.dataSource = self
        upcomingTasks.delegate = self
        upcomingTasks.tableFooterView = UIView()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews(){
        upcomingTasks.frame = CGRect(x: upcomingTasks.frame.origin.x, y: upcomingTasks.frame.origin.y, width: upcomingTasks.frame.size.width, height: upcomingTasks.contentSize.height)
        upcomingTasks.reloadData()
    }
}

extension DailyTasksViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? CalenderCollectionViewCell{
            //cell.name.text = "Suh"
            return cell
        }
        
        return CategoryCollectionViewCell()
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
}

extension DailyTasksViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? UpcomingTaskTableViewCell {
            
            return cell
        }
        
        return UpcomingTaskTableViewCell()
    }
    

    
    
}
