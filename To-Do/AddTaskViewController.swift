//
//  AddTaskViewController.swift
//  To-Do
//
//  Created by Sushanth S on 27/09/21.
//

import UIKit
import DropDown

class AddTaskViewController: UIViewController {

    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var InnerView: UIView!
    @IBOutlet weak var time: UIDatePicker!
    @IBOutlet weak var category: UITextField!
    @IBOutlet weak var dropdown: UIButton!
    @IBOutlet weak var buttonStack: UIStackView!
    @IBOutlet weak var date: UIDatePicker!
    @IBOutlet weak var endTime: UIDatePicker!
    @IBOutlet weak var startTime: UIDatePicker!

    let categories = Category.allCases.map { $0.rawValue }
    override func viewDidLoad() {
        super.viewDidLoad()
        category.rightView = dropdown
        category.rightViewMode = .always
       // time.datePickerMode = .time
        outerView.viewSetUp()
        outerView.addShadow(offset: CGSize(width: 1, height: 1), color: .gray, radius: 10, opacity: 0.6)
        InnerView.viewSetUp()
        //category.addrightButton()
        addrightButton()
        date.datePickerMode = .date
        startTime.datePickerMode = .time
        
        endTime.datePickerMode = .time
        endTime.locale = Locale(identifier: "en_g")
        self.buttonStack.isHidden = true

            
        
        
        
    }
    func timeInRequiredFormat(time: UIDatePicker) -> String {
        
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        
        return formatter.string(from: time.date)
    }
    
    @IBAction func addTask(_ sender: UIButton) {
        
        print("start time == \(timeInRequiredFormat(time: startTime))")
    }
    @IBAction func dropdownAction(_ sender: UIButton) {
        self.buttonStack.isHidden = !self.buttonStack.isHidden
    }
    
    @IBAction func dateSeleted(_ sender: Any) {
      
    }
    
    @IBAction func startTime(_ sender: UIDatePicker) {
        print(sender.timeZone)
        let time = "\(sender.date)"
        let timeSelected = time.components(separatedBy: " ")
        print("Time=======\(timeSelected)")
        
    }
    
    
    
    func addrightButton() {
        
        for i in 0 ... categories.count - 1 {
            
            print(i)
            let option = UIButton()
            option.setTitle(categories[i], for: .normal)
            option.backgroundColor = UIColor.white
            option.tintColor = UIColor.white
            option.titleLabel?.textColor = .red
            option.setTitleColor(.black, for: .normal)
            option.translatesAutoresizingMaskIntoConstraints = false
            option.addTarget(self, action: #selector(dropdownSeleted), for: .touchUpInside)
            option.tag = i + 1

            buttonStack.alignment = .fill
            buttonStack.distribution = .fillEqually
            buttonStack.spacing = 0
            buttonStack.addArrangedSubview(option)         }
      
    }
    
    @objc func dropdownSeleted(sender: UIButton!) {
        print("buttom os created")
        UIStackView.transition(with: sender, duration: 0.8,
                          options: .curveEaseInOut,
                          animations: {
                            self.buttonStack.isHidden = true
                            
                          })
        category.text = sender.titleLabel?.text
        
    }
    
    @IBAction func back(_ sender: UIButton) {
    }
    
}

extension UITextField {
    
    
}
